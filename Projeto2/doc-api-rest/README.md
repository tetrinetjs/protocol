# Documentação da API REST do Protocolo

A documentação foi feita com swagerr 2.0 com o node@10 para testar a api pelo swagger faça o clone do repositório e execute os seguintes comando dentro do diretório *doc-api-rest*

* ``npm install``
* ``swagger project start``
* ``swagger project edit``


Se for necessário instalar o swagger siga os passos do tutorial swagger - [tutorial swagger](https://scotch.io/tutorials/speed-up-your-restful-api-development-in-node-js-with-swagger)

É recomendável usar o Swagger com o node@10 a versão node@12 não é compatível com o Swagger.