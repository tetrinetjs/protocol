# Protocolo TetriNET / WebSocket TetriNET

## Protocolo TetriNET Game RFC 20191- [**ACESSE O PROTOCOLO COMPLETO AQUI**](https://www.overleaf.com/read/mjqdjtmgztzr)

## Protocolo WebSocket TetriNET RFC 20192- [**ACESSE O PROTOCOLO COMPLETO AQUI**](https://www.overleaf.com/read/gjrzwwbwmxxt)

## Artefatos do Projeto

* O código fonte do protocolo foi desenvolvido em LaTex no editor LaTex online [Overleaf](https://www.overleaf.com). 

* Projeto 1 - Arquivo *tetrinetProtocol.tex*, localizado em [Projeto1/tetrinetProtocol.tex](https://gitlab.com/tetrinetjs/protocol/tree/master/Projeto1).

 * Projeto 2 - Arquivo *websocket_tetrinet_rfc.tex*, localizado em [Projeto2/websocket_tetrinet_rfc.tex](https://gitlab.com/tetrinetjs/protocol/tree/master/Projeto2).

* Documentação da API REST do Projeto 2 - Diretório *doc-api-rest*, localizado em [Projeto2/doc-api-rest](https://gitlab.com/tetrinetjs/protocol/tree/master/Projeto2/doc-api-rest).

* Documentação da API Real Time do Projeto 2 - Diretório *doc-api-rest*, localizado em [Projeto2/doc-api-realtime](https://gitlab.com/tetrinetjs/protocol/tree/master/Projeto2/doc-api-rest).

## Objetivos:
* Analisar o protocolo do jogo tetriNET
* Documentar o protocolo do jogo tetriNET
* Documentar o protocolo Webservice do jogo tetriNET

## Grupo 9 - membros
* Fernando Ferreira da Costa (Líder)
* Julio Alfredo Moreira Sousa Junior (Desenvolvedor)
* Ismael (Desenvolvedor)
* João (Desenvolvedor)

## Observações
Demais definições encontram-se nos arquivos READMEs dos diretórios do projeto.