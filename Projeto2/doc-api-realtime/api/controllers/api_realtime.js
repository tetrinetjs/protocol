'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  move_peace: move_peace,
  special: special,
  pause: pause,
  message: message
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function move_peace(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var peace = req.swagger.params.peace.value || 0;
  var moviment = req.swagger.params.moviment.value || '';
  var playernum = req.swagger.params.playernum.value || '';
  var result = ''

  if(peace < 1 || peace > 7) peace = 'peça inválida'

  switch (moviment){
    case 'click_right': 
      break
    case 'click_left':
      break
    case 'click_up':
      break
    case 'click_down':
      break
    default:
      moviment = 'movimento inválido' 
  }
  if(playernum < 1 || playernum > 6) playernum = 'jogador inválido' 
  

  if(moviment !== 'movimento inválido' && playernum !== 'jogador inválido' && peace !== 'peça inválida'){
    if(peace === 1){
      result = util.format(`{"action_get": [{"status": "ok", "info": "success"},
      {"action_set":{"peace": "peça l (Linha) moveu", "moviment": "${moviment}"}},
      {"blockset_update":{"code": "f", "playernum": "${playernum}", "new_blockset": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000500000055511101000a555511010005555a5101022r525521010225222c22"}}]}`) 
    }
    else if(peace === 2){
      result = util.format(`{"action_get": [{"status": "ok", "info": "success"},
      {"action_set":{"peace": "peça O (Quadrado) moveu", "moviment": "${moviment}"}},
      {"blockset_update":{"code": "f", "playernum": "${playernum}", "new_blockset": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000500000055511101000a555511010005555a5101022r525521010225222c22"}}]}`) 
    }
    else if(peace === 3){
      result = util.format(`{"action_get": [{"status": "ok", "info": "success"},
      {"action_set":{"peace": "peça J (Esquerda-L) moveu", "moviment": "${moviment}"}},
      {"blockset_update":{"code": "f", "playernum": "${playernum}", "new_blockset": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000500000055511101000a555511010005555a5101022r525521010225222c22"}}]}`) 

    }
    else if(peace === 4){
      result = util.format(`{"action_get": [{"status": "ok", "info": "success"},
      {"action_set":{"peace": "peça Direita-L moveu", "moviment": "${moviment}"}},
      {"blockset_update":{"code": "f", "playernum": "${playernum}", "new_blockset": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000500000055511101000a555511010005555a5101022r525521010225222c22"}}]}`) 

    }
    else if(peace === 5){
      result = util.format(`{"action_get": [{"status": "ok", "info": "success"},
      {"action_set":{"peace": "peça Direita-Z moveu", "moviment": "${moviment}"}},
      {"blockset_update":{"code": "f", "playernum": "${playernum}", "new_blockset": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000500000055511101000a555511010005555a5101022r525521010225222c22"}}]}`) 

    }
    else if(peace === 6){
      result = util.format(`{"action_get": [{"status": "ok", "info": "success"},
      {"action_set":{"peace": "peça S (Esquerda-Z) moveu", "moviment": "${moviment}"}},
      {"blockset_update":{"code": "f", "playernum": "${playernum}", "new_blockset": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000500000055511101000a555511010005555a5101022r525521010225222c22"}}]}`) 

    }
    else 
      result = util.format(`{"action_get": [{"status": "ok", "info": "success"},
        {"action_set":{"peace": "peça T (Meia Cruz) moveu", "moviment": "${moviment}"}},
        {"blockset_update":{"code": "f", "playernum": "${playernum}", "new_blockset": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000500000055511101000a555511010005555a5101022r525521010225222c22"}}]}`) 

  }
  else result = util.format(`{"action_get": {"status": "erro", "info": "peça, jogador ou movimento inválido(s)"}}`) 
  
   

  // this sends back a JSON response which is a single string
  res.json(result);
}

function special(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var specialtype = req.swagger.params.specialtype.value || '';
  var targetnum = req.swagger.params.targetnum.value || 0;
  var sendernum = req.swagger.params.sendernum.value || 0;
  var result = ''
  
  if(targetnum < 1 || targetnum > 6) targetnum = 'jogador inválido'
  if(sendernum < 1 || sendernum > 6) sendernum = 'jogador inválido'

  switch (specialtype){
    case 'a': 
      break
    case 'c':
      break
    case 'n':
      break
    case 'r':
      break
    case 's':
      break
    case 'b':
      break
    case 'g':
      break
    case 'q':
      break
    case 'o':
      break
    default:
      specialtype = 'special inválido'
  }

  if(specialtype !== 'special inválido' && targetnum !== 'jogador inválido' && sendernum !== 'jogador inválido'){
    result = util.format(`{"special_get": [{"status": "ok", "info": "success"},
    {"special_set":{"code": "sb", "targetnum": "${targetnum}", "specialtype": "${specialtype}", "sendernum": ${sendernum}}},
    {"blockset_update":{"code": "f", "playernum": "${targetnum}", "new_blockset": "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000500000055511101000a555511010005555a5101022r525521010225222c22"}}]}`) 

  }
  else result = util.format(`{"special_get": {"status": "erro", "info": "especial ou jogador inválido(s)"}}`) 

  // this sends back a JSON response which is a single string
  res.json(result);
}

function pause(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var state = req.swagger.params.state.value || '';
  var result = ''
  
  if(state !== 1 && state !== 2) state = 'estado inválido'
  
  if(state !== 'estado inválido' && state === 1){
    result = util.format(`{"pause_get": [{"status": "ok", "info": "success"},
    {"pause_set":{"code": "pause", "state": "${state}"}},
    {"pause_update":{"code": "pause", "state": "${state}", "info": "Jogo Pausado"}}]}`) 

  }else if(state !== 'estado inválido' && state === 2){
    result = util.format(`{"pause_get": [{"status": "ok", "info": "success"},
    {"pause_set":{"code": "pause", "state": "${state}"}},
    {"pause_update":{"code": "pause", "state": "${state}", "info": "Jogo Retomado"}}]}`) 

  }
  else result = util.format(`{"pause_get": {"status": "erro", "info": "estado inválido"}}`) 

  // this sends back a JSON response which is a single string
  res.json(result);
}

function message(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var message = req.swagger.params.message.value || '';
  var result = ''
  
  
  
    result = util.format(`{"message_get": [{"status": "ok", "info": "success"},
    {"message_set":{"code": "gmsg", "message": "${message}"}},
    {"message_update":{"code": "gmsg", "message": "${message}"}}]}`) 

  // this sends back a JSON response which is a single string
  res.json(result);
}