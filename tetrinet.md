PROXY
publish action/{channel_id}/{user_id}/game/update_blockset
Envia o blockset atualizado para um determinado cliente de 1 em 1 segundo.
Mensagem: STRING

PROXY 
publish action/{channel_id}/game/update_blockset
Envia a atualização de um determinado jogador para todos os jogares. 
Mensagem: STRING

PROXY
publish action/{channel_id}/chat/send_messages
Envia mensagem de chat de um jogador para todos os jogares.

CLIENT
publish action/{channel_id}/game/on_piece
Envia informação sobre ação que o usuário deseja fazer na peça (UP, DOWN, RIGHT, LEFT)
Mensagem: STRING (‘U’, ‘D’, ‘R’, ‘L’)
+ U: A peça será rotacionada
+ D: A peça deverá descer mais rápido
+ R: A peça deverá ser movida para o lado direito
+ L: A peça deverá ser movida para o lado esquerdo

CLIENT
subscribe action/{channel_id}/game/chat_messages
Recebe mensagens de chat de todos os jogadores

